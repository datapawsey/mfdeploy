#!/usr/bin/env python3

import os
import sys
import time
import inspect
import logging
import getpass
import argparse
import configparser as ConfigParser
import mfclient.mfclient as mfclient

# ---
class modules:
    global mf_client

    def www_html(self, config, section):
# must locally be in www/ so it matches the server
        local_path = os.path.abspath("www")
        print("WWW_HTML: Deploying code from: %s" % local_path)
        upload_list = []
        parent = os.path.normpath(os.path.join(local_path, ".."))
        for root, directory_list, name_list in os.walk(local_path):
# CURRENT - os.path.relpath must be converted to a posix path for those people deploying from windows...
            tmp_path = os.path.relpath(path=root)
            tmp_path = tmp_path.replace('\\', '/')
            remote = "/" + tmp_path
            upload_list.extend( [(remote, os.path.normpath(os.path.join(os.getcwd(), root, name))) for name in name_list] )
# upload
# FIXME - this is very, very slow at the moment ... 
            for namespace, local_filepath in upload_list:
# NB: wtf - have to overwrite as Arcitecta have their own index.html
#                mf_client.put(namespace, local_filepath, overwrite=False)
                mf_client.put(namespace, local_filepath)

        print("\nWWW_HTML: Publishing ...")
#        mf_client.aterm_run("asset.query :where \"namespace>='/www'\" :action pipe :service -name asset.label.add < :label PUBLISHED >")
# NEW - performance improvement
        mf_client.aterm_run('asset.query :namespace "/www" :action pipe :service -name asset.label.add < :label PUBLISHED >')

# obsolete
#        mf_client.aterm_run("asset.query :where \"namespace='/www' and name='system-alert'\" :action pipe :service -name asset.label.remove < :label PUBLISHED >")

#        print("WWW_HTML: Menu Translation Processors ...")
# FIXME - these are incompatible with production 
# NOTE - it's probably the MISSING encryption required=True flag
#eg Exception: Error from server: A processor has already been added with the url '/projects' with different settings
#        xml = mf_client.aterm_run("http.processor.create :ifexists quiet :url /projects :type asset :translate /www :authentication < :domain www-public :user www-public >") 
#        xml = mf_client.aterm_run("http.processor.create :ifexists quiet :url /public :type asset :translate /www :authentication < :domain www-public :user www-public >")
#        xml = mf_client.aterm_run("http.processor.create :ifexists quiet :url /tools :type asset :translate /www :authentication < :domain www-public :user www-public >")
#        xml = mf_client.aterm_run("http.processor.create :ifexists quiet :url /news :type asset :translate /www :authentication < :domain www-public :user www-public >")
#        xml = mf_client.aterm_run("http.processor.create :ifexists quiet :url /help :type asset :translate /www :authentication < :domain www-public :user www-public >")

# this is obsolete with namespace quotas
#        print("WWW_HTML: Quota Administration ...")
#        xml = mf_client.aterm_run("authorization.role.create :ifexists ignore :role quota-administrator :description \"Administration role for storage requests\"")
#        xml = mf_client.aterm_run("actor.grant :type role :name quota-administrator :perm < :access ADMINISTER :resource -type service asset.store.arg.set >")

        print("WWW_HTML: Done")

# CHECK - processors -= flag_encrypt required ... ???
#    mf_execute("http.processor.create :ifexists quiet :url /projects :type asset :translate /www :authentication < :domain www-public :user www-public > :encryption-required %s" % flag_encrypt)
#    mf_execute("http.processor.create :ifexists quiet :url /public :type asset :translate /www :authentication < :domain www-public :user www-public > :encryption-required %s" % flag_encrypt)
#    mf_execute("http.processor.create :ifexists quiet :url /tools :type asset :translate /www :authentication < :domain www-public :user www-public > :encryption-required %s" % flag_encrypt)
#       mf_execute("http.processor.create :ifexists quiet :url /news :type asset :translate /www :authentication < :domain www-public :user www-public > :encryption-required %s" % flag_encrypt)
#    mf_execute("http.processor.create :ifexists quiet :url /help :type asset :translate /www :authentication < :domain www-public :user www-public > :encryption-required %s" % flag_encrypt)


    def www_scripts(self, config, section):
        local = config.get("scripts", "local")
        remote = config.get("scripts", "remote")
        domain = config.get("access", "name")
        root_path = os.path.abspath(local)

# upload all scripts
        print("WWW_SCRIPTS: Uploading from %s" % root_path)
        script_list = ["www_list.tcl"]
        for script in script_list:
            local_filepath = os.path.join(root_path, script)
            mf_client.put(remote, local_filepath, overwrite=True)

        print("WWW_SCRIPTS: Configuring service calls")
# configure individual service calls
        xml = mf_client.aterm_run("asset.query :where \"namespace=%s and name='www_list.tcl'\"" % remote)
        elem = xml.find(".//id")
        mf_client.aterm_run("asset.label.add :id %s :label PUBLISHED" % elem.text)

# OMG - this works ...
        mf_client.aterm_run("system.service.add :name www.list :replace-if-exists true :access ACCESS :object-meta-access ACCESS :object-data-access ACCESS :definition < :element -name namespace -type string :element -name page -type long -min-occurs 0 -default 1 :element -name size -type long -min-occurs 0 -default 20 :element -name filter -type string -min-occurs 0 -default \"*\" :element -name recurse -type boolean -min-occurs 0 -default false > :execute \"return [xvalue result [asset.script.execute :id %s :arg -name namespace [xvalue namespace $args] :arg -name page [xvalue page $args] :arg -name size [xvalue size $args] :arg -name filter [xvalue filter $args] :arg -name recurse [xvalue recurse $args]]]\"" % elem.text)

        mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service www.list >" % domain)
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service www.list >")

        print("WWW_SCRIPTS: Done")


#--- project provisioning - custom service calls and permissions
    def projects(self, config, section):
        local = config.get("scripts", "local")
        remote = config.get("scripts", "remote")
        root_path = os.path.abspath(local)
        print("PROJECTS: Uploading from %s" % root_path)

# FIXME: production already has a project describe - with different syntax - that the web code uses ...
        script_list = ["project_create.tcl", "project_describe.tcl", "project_destroy.tcl", "project_grant.tcl", "project_modify.tcl", "project_revoke.tcl", "project_list.tcl", "project_get.tcl"]
        script_ids = {}
        for script in script_list:
            local_filepath = os.path.join(root_path, script)
            reply = mf_client.put(remote, local_filepath, overwrite=True)
# get the script id
            xml_reply = mf_client.aterm_run('asset.identifier.get :path "%s/%s"' % (remote, script))
            elem = xml_reply.find(".//id")
            script_ids[script] = int(elem.text)

# legacy web interface uses this
        print("PROJECTS: Configuring service calls")

#                mf_client.debug = 2

        mf_client.aterm_run('system.service.add :name "project.describe" :replace-if-exists true :description "Custom project description for the web portal." :access ACCESS :definition < :element -name name -type string -min-occurs 0 -default "*" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] ]]"' % script_ids["project_describe.tcl"])

        mf_client.aterm_run('system.service.add :name "project.create" :replace-if-exists true :description "Custom project creation." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string :element -name store -type string :element -name quota -type string :element -name description -type string -min-occurs 0 -default "None" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name store [xvalue store $args] :arg -name quota [xvalue quota $args] :arg -name description [xvalue description $args] ]]"' % script_ids["project_create.tcl"])

        mf_client.aterm_run('system.service.add :name "project.modify" :replace-if-exists true :description "Modify project settings." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string :element -name description -type string -min-occurs 0 -default "1" :element -name quota -type string -min-occurs 0 -default "2" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name description [xvalue description $args] :arg -name quota [xvalue quota $args] ]]"' % script_ids["project_modify.tcl"])

# seems the behaviour has changed slightly here ... empty string are no longer ok for a default (the $args xvalue extract attempt will fail)
        mf_client.aterm_run('system.service.add :name "project.grant" :replace-if-exists true :description "Grant project membership." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string :element -name administer -type string -min-occurs 0 -default "1" :element -name readwrite -type string -min-occurs 0 -default "2" :element -name readonly -type string -min-occurs 0 -default "3" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name administer [xvalue administer $args] :arg -name readwrite [xvalue readwrite $args] :arg -name readonly [xvalue readonly $args] ]]"' % script_ids["project_grant.tcl"])

        mf_client.aterm_run('system.service.add :name "project.revoke" :replace-if-exists true :description "Revoke project membership." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string :element -name administer -type string -min-occurs 0 -default "1" :element -name readwrite -type string -min-occurs 0 -default "2" :element -name readonly -type string -min-occurs 0 -default "3" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name administer [xvalue administer $args] :arg -name readwrite [xvalue readwrite $args] :arg -name readonly [xvalue readonly $args] ]]"' % script_ids["project_revoke.tcl"])

        mf_client.aterm_run('system.service.add :name "project.destroy" :replace-if-exists true :description "Destroy a project." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string :element -name destroy-data -type boolean -min-occurs 0 -default false > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name destroy [xvalue destroy-data $args] ]]"' % script_ids["project_destroy.tcl"])

        mf_client.aterm_run('system.service.add :name "project.get" :replace-if-exists true :description "Get project details." :access ACCESS :object-meta-access ACCESS :definition < :element -name name -type string -min-occurs 0 -default all :element -name show-members -type boolean -min-occurs 0 -default false > :execute "return [xvalue result [asset.script.execute :id %d :arg -name name [xvalue name $args] :arg -name members [xvalue show-members $args] ]]"' % script_ids["project_get.tcl"])

# FIXME: production already has a project.list - with different syntax - that the web code uses ...
        mf_client.aterm_run('system.service.add :name "project.list" :replace-if-exists true :description "List projects." :access ACCESS :definition < :element -name filter -type string -min-occurs 0 -default "*" > :execute "return [xvalue result [asset.script.execute :id %d :arg -name filter [xvalue filter $args] ]]"' % script_ids["project_list.tcl"])

        print("PROJECTS: Configuring permissions")

        try:
# FIXME - relies on info from access section ... can we untangle?
            domain = config.get("access", "name")
# CURRENT - new MF bug ... can't explicitly grant access to project.list ... but project.* will work just fine
            mf_client.aterm_run('actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service project.* >' % domain)
            mf_client.aterm_run('asset.namespace.acl.grant :namespace %s :acl < :actor -type domain %s :access < :namespace access :namespace execute :asset access :asset-content access > >' % (remote, domain))

        except Exception as e:
            print("PROJECTS: ERROR: %s" % str(e))


# NEW - create a role for executive viewing
        mf_client.aterm_run('authorization.role.create :role system-monitor :ifexists "ignore"')
# custom service call visibility
#        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service project.list >')
#        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service project.describe >')
# TODO - underlying permisions
#        mf_client.aterm_run("asset.namespace.acl.grant :namespace / :acl < :actor -type role system-monitor :access < :namespace access :namespace execute > >")
#        mf_client.aterm_run("asset.namespace.acl.grant :namespace /projects :acl < :actor -type role system-monitor :access < :namespace access > >")

        mf_client.aterm_run('actor.grant :name system-monitor :type role :role -type role read-only')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service server.status >')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service network.describe >')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service authorization.role.namespace.list >')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service authorization.role.namespace.describe >')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service actors.granted >')
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ACCESS :resource -type service authentication.user.list >')
# I this is the only way to grant ability to view membership in a role namespace ... ugh
        mf_client.aterm_run('actor.grant :name system-monitor :type role :perm < :access ADMINISTER :resource -type "role:namespace" * >')

        print("PROJECTS: Done")


#--- project backing store(s)
# NB: this won't work on production as the store base directories already exist (mediaflux demands they don't - so it can create)
    def stores(self, config, section):

        print("STORES: Adding stores")

        name_list = config.get(section, "name_list").split(",")
        type_list = config.get(section, "type_list").split(",")
        path_list = config.get(section, "path_list").split(",")

# CURRENT - don't create as a store - just plonk in the settings metadata for the /projects namespace 
        document = "<"
        for name, path, fstype in zip(name_list, path_list, type_list):
            print("STORES: [%s][%s][%s]" % (name, fstype, path))
#                   mf_client.aterm_run("asset.store.create :name %s :type %s :path \"%s\"" % (name, fstype, path))
            document += " :%s < :type %s :path %s >" % (name, fstype, path)
        document += " >"

        print("[%s]" % document)

        try:
                    mf_client.aterm_run("asset.namespace.application.settings.set :namespace /projects :app stores :settings %s" % document)
        except Exception as e:
            print(str(e))

        print("STORES: Done")


#--- main mediaflux access config
    def access(self, config, section):
        label = config.get(section, "type")
        name = config.get(section, "name")
        projects = config.get(section, "projects")
        scripts = config.get("scripts", "remote")

# NEW - support a local flavour (for testing) as well
        if 'ldap' in label.lower():
            print("ACCESS: ldap branch")
            host = config.get(section, "host")
            port = config.get(section, "port")
            path = config.get(section, "path")

# --- provider
            xml = mf_client.aterm_run("ldap.provider.exists :provider %s" % name)
            value = xml.find(".//exists")
            if "true" in value.text:
                print("ACCESS: modifying provider=%s" % name)
                xml = mf_client.aterm_run("ldap.provider.modify :provider %s :host %s :port %s" % (name, host, port))
            else:
                print("ACCESS: deploying provider=%s" % name)
# TODO - make optional
                options = config.get(section, "options")
                xml = mf_client.aterm_run("ldap.provider.add :name %s :host %s :port %s %s" % (name, host, port, options))

# --- domain
            xml = mf_client.aterm_run("authentication.domain.exists :domain %s" % name)
            value = xml.find(".//exists")
            if "true" in value.text:
                print("ACCESS: modifying domain=%s" % name)
                xml = mf_client.aterm_run("authentication.ldap.domain.modify :domain %s :provider %s :user < :path %s >" % (name, name, path))
            else:
                print("ACCESS: deploying domain=%s" % name)
                xml = mf_client.aterm_run("authentication.ldap.domain.create :domain %s :provider %s :user < :path %s >" % (name, name, path))

        elif 'local' in label.lower():
            print("ACCESS: local branch")
            user = config.get(section, "user")
            password = config.get(section, "password")
            xml = mf_client.aterm_run("authentication.domain.create :domain %s :ifexists ignore" % name)
            xml = mf_client.aterm_run("authentication.user.create :domain %s :user %s :password %s" % (name, user, password))

        else:
            print("ACCESS: unknown type, skipping...")
            return



# --- set base level domain service call permissions
        print("ACCESS: service call permissions")
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"actor.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"actors.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"asset.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access MODIFY :resource -type service \"asset.*\" >" % name)
# could be a bug - this needs to be done explicitly (wildcard doesn't seem to capture)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access MODIFY :resource -type service asset.destroy >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access MODIFY :resource -type service asset.namespace.destroy >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"audit.*\" >" % name)
# 13/7/2016 - Arcitecta have changed audit.query to now require ADMINISTER ... wtf - it's a QUERY
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ADMINISTER :resource -type service audit.query >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"authentication.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"authorization.*\" >" % name)
# NEW - aterm requires this
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service network.self.describe >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"secure.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access MODIFY :resource -type service \"secure.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"system.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type service \"user.*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access ACCESS :resource -type document \"*\" >" % name)
        xml = mf_client.aterm_run("actor.grant :name %s :type domain :perm < :access MODIFY :resource -type document \"*\" >" % name)

# --- create required namespaces that are not part of vanilla mediaflux
        print("ACCESS: namespace permissions")
        try:
            xml = mf_client.aterm_run("asset.namespace.create :namespace %s" % projects)
        except:
            print("ACCESS: %s already exists" % projects)
        try:
            mf_client.aterm_run("asset.namespace.create :namespace %s" % scripts)
            mf_client.aterm_run("asset.namespace.visibility.set :namespace %s :visible-to < :actor -type domain system >" % scripts)
        except:
            print("ACCESS: %s already exists" % scripts)

# --- set base namespace permission
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace / :acl < :actor -type domain mflux :access < :namespace access :namespace execute :asset access :asset-content access > >")
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace / :acl < :actor -type domain %s :access < :namespace access :namespace execute :asset access > >" % name)
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace / :acl < :actor -type domain www-public :access < :namespace access :namespace execute > >") 
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace /www :acl < :actor -type domain %s :access < :namespace access :namespace execute :asset access :asset-content access > >" % name)
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace /www :acl < :actor -type domain www-public :access < :namespace access :namespace execute :asset access :asset-content access > >")
        xml = mf_client.aterm_run("asset.namespace.acl.grant :namespace %s :acl < :actor -type domain %s :access < :namespace access :namespace execute :asset access > >" % (projects, name))

        print("ACCESS: Done")


# --- public access setup
    def public(self, config, section):

        projects = config.get("access", "projects")
        scripts = config.get("scripts", "remote")

        xml = mf_client.aterm_run("authentication.domain.exists :domain public")
        value = xml.find(".//exists")
        if "true" in value.text:
            print("PUBLIC: domain already exists")
        else:
            print("PUBLIC: creating domain")
            mf_client.aterm_run("authentication.domain.create :domain public")

        xml = mf_client.aterm_run("authentication.user.exists :user public :domain public")
        value = xml.find(".//exists")
        if "true" in value.text:
            print("PUBLIC: user exists")
        else:
            print("PUBLIC: creating user")
            mf_client.aterm_run("authentication.user.create :user public :domain public :password public")

        print("PUBLIC: Setting service permissions")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service user.self.settings.get >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service system.logoff >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.get >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.script.execute >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.namespace.list >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.namespace.exists >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.query >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.label.exists >")
        mf_client.aterm_run("actor.grant :name public :type domain :perm < :access ACCESS :resource -type service asset.content.status >")
        mf_client.aterm_run("actor.grant :name public :type domain :role -type role read-only")
#            mf_client.aterm_run("actor.grant :name public:public :type user :role -type role read-only")

        print("PUBLIC: Setting namespace permissions")
        mf_client.aterm_run("asset.namespace.acl.grant :namespace / :acl < :actor -type domain public :access < :namespace access :namespace execute > >") 
        mf_client.aterm_run("asset.namespace.acl.grant :namespace %s :acl < :actor -type domain public :access < :namespace access :namespace execute > >" % projects)
        mf_client.aterm_run("asset.namespace.acl.grant :namespace %s :acl < :actor -type domain public :access < :namespace access :namespace execute :asset access :asset-content access > >" % scripts)

        print("PUBLIC: Setting published view")
        try:
                mf_client.aterm_run("asset.view.create :name projects-published :assetacl false :rule -path / < :namespace %s :label PUBLISHED :queryable true >" % projects)
                mf_client.aterm_run("asset.view.associate :domain public :user public :view projects-published")
        except Exception as e:
            print(str(e))

        print("PUBLIC: Done")


    def email(self, config, section):
# --- required settings
        host = config.get(section, "host")
        src = config.get(section, "from")
        warn = config.get(section, "warn")
        crash = config.get(section, "crash")
        print("EMAIL: SMTP host")
        xml = mf_client.aterm_run("server.property.set :property -name mail.smtp.host %s" % host)
        print("EMAIL: Notification settings")
        xml = mf_client.aterm_run("server.property.set :property -name mail.from %s" % src)
        xml = mf_client.aterm_run("server.property.set :property -name mail.critical.issue.to %s" % warn)
        xml = mf_client.aterm_run("server.property.set :property -name mail.crash.to %s" % crash)
        print("EMAIL: OK")


# ---
if __name__ == '__main__':

    mymodules = modules()

# server config (section heading) to use
    p = argparse.ArgumentParser(description='mfdeploy help')
    p.add_argument('-c', dest='config', default='default', help='Mediaflux settings file (eg mediaflux.cfg)')
    p.add_argument('-m', dest='module', default='', help='The module to install')
    if len(sys.argv)==1:
        p.print_help()
        print("Available modules:\n  'all'")
        for member in inspect.getmembers(mymodules, inspect.ismethod):
            print("  '%s'" % member[0])
        print()
        exit(0)

    args = p.parse_args()

# init mediaflux server connection
    config = ConfigParser.ConfigParser()
    config_filepath = args.config
    config.read(config_filepath)

    current = 'mediaflux'

    server = config.get(current, 'server')
    protocol = config.get(current, 'protocol')
    port = config.get(current, 'port')
    domain = config.get(current, 'domain')

# make it easy for automated deployment
    user = config.get(current, 'user')
    password = None
    if config.has_option(current, 'password'):
        password = config.get(current, 'password')
# container delay (allow mediaflux time to fire up) 
    delay=0
    if config.has_option(current, 'delay'):
        delay = int(config.get(current, 'delay'))
    if config.has_option(current, 'debug'):
        logging.basicConfig(format='%(levelname)9s %(asctime)-15s >>> %(module)s.%(funcName)s(): %(message)s', level=logging.INFO)

# establish authenticated connection
    print("Mediaflux server: %r : %r" % (protocol, server))
    if delay > 0:
        print("Waiting for server to come up, please be patient...")
        time.sleep(delay)

    mf_client = mfclient.mf_client(protocol=protocol, port=port, server=server, domain=domain)
    if mf_client.connect() is False:
        if password is not None:
            mf_client.login(user, password)
        else:
            password = getpass.getpass("Enter %s:%s password: " % (domain, user))
            mf_client.login(user, password)

# special case
    if args.module == 'all':
        print("Configuring ALL...")
        for member in ["access", "projects", "stores", "public", "email", "www_html", "www_scripts"]:
#        for member in inspect.getmembers(mymodules, inspect.ismethod):
#            print("running: %s" % member[0])
            if hasattr(mymodules, member):
                method = getattr(mymodules, member)
                result = method(config, member)

        exit(0)

# call the module
    if hasattr(mymodules, args.module):
        method = getattr(mymodules, args.module)
        result = method(config, args.module)

    else:
        print("No such module [%s]" % args.module)
        print("Available modules:\n  'all'")
        for member in inspect.getmembers(mymodules, inspect.ismethod):
            print("  %r" % member[0])

