
Mediaflux configuration management tool

Covers Pawsey specifics for:
* base configuration (ldap, stores, etc)
* the custom web portal


Dependencies:
 * mfclient
 * mfscripts
 * portal html

Owner:
sean.flemin@pawsey.org.au